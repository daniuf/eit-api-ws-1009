<?php

ini_set('date.timezone', 'America/Argentina/Buenos_Aires');

require_once('libreria.php');

define("WS", "https://jsonplaceholder.typicode.com");

$url = WS.'/posts/1';
$method = 'DELETE';

$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

$respuesta = curl_exec($ch);//Ejecuto peticion

$info = curl_getinfo($ch);//Pido info de esa peticion

if ($info['http_code'] == 200) {

  echo "El recurso ha sido eliminado con exito";

} else {
  loguear("logs/error.log", "a+", "Ha ocurrido un error al realizar la peticion a ".$url);
  loguear("logs/error.log", "a+", "Metodo Utilizado: ".$method);
  loguear("logs/error.log", "a+", "Status Code Devuelto: ".$info['http_code']);
  echo "Error al crear recurso";
}

curl_close($ch);

