<?php

ini_set('date.timezone', 'America/Argentina/Buenos_Aires');

require_once('libreria.php');

define("WS", "https://jsonplaceholder.typicode.com");

$url = WS.'/posts';
$method = 'POST';

$json_input = array(
		'title' => "Este es el titulo",
		'body' => "Este es el cuerpo del post",
		'userId' => 33
		);

$json = json_encode($json_input);

$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

$respuesta = curl_exec($ch);//Ejecuto peticion

$info = curl_getinfo($ch);//Pido info de esa peticion

if ($info['http_code'] == 201) {

  $json_decode = json_decode($respuesta);

  if ($json_decode) {
    //Aca hago lo que tenga que hacer con esa info
    var_dump($json_decode);
  }
} else {
  loguear("logs/error.log", "a+", "Ha ocurrido un error al realizar la peticion a ".$url);
  loguear("logs/error.log", "a+", "Metodo Utilizado: ".$method);
  loguear("logs/error.log", "a+", "Status Code Devuelto: ".$info['http_code']);
  echo "Error al crear recurso";
}

curl_close($ch);

