<?php

ini_set('date.timezone', 'America/Argentina/Buenos_Aires');

require_once('libreria.php');

define("WS", "https://api.estadisticasbcra.com");
define("TOKEN", 'BEARER eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2MzE5MTgyNzgsInR5cGUiOiJleHRlcm5hbCIsInVzZXIiOiJkYW5pdWZAZ21haWwuY29tIn0.V_GhfClDsc11nvOTUaZ6inKVEQ4YH5mM5ynhTSBw4gT9-g9kpLoqBtN3OQ-ebmdrpnTdFcJJVshliCrmzpWAiQ');

$url = WS.'/usd_of';

$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization: '.TOKEN));
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

$json = curl_exec($ch);//Ejecuto peticion

$info = curl_getinfo($ch);//Pido info de esa peticion

if ($info['http_code'] == 200) {

  $json_decode = json_decode($json);

  if ($json_decode) {
    //Aca hago lo que tenga que hacer con esa info
    for ($i = 0; $i < count($json_decode); $i++) {
      echo "<p>La cotizacion para el dia ".$json_decode[$i]->d. " fue de ".$json_decode[$i]->v."</p>";
    }
  }
} else {
  loguear("logs/error.log", "a+", "Ha ocurrido un error al realizar la peticion a ".$url);
  loguear("logs/error.log", "a+", "Metodo Utilizado: GET");
  loguear("logs/error.log", "a+", "Status Code Devuelto: ".$info['http_code']);
  echo "Ha ocurrido un error";
}

curl_close($ch);

