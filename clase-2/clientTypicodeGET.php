<?php

ini_set('date.timezone', 'America/Argentina/Buenos_Aires');

require_once('libreria.php');

define("WS", "https://jsonplaceholder.typicode.com");

$url = WS.'/posts';

$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

$json = curl_exec($ch);//Ejecuto peticion

$info = curl_getinfo($ch);//Pido info de esa peticion

if ($info['http_code'] == 200) {

  $json_decode = json_decode($json);

  if ($json_decode) {
    //Aca hago lo que tenga que hacer con esa info
    var_dump($json_decode);
  }
} else {
  loguear("logs/error.log", "a+", "Ha ocurrido un error al realizar la peticion a ".$url);
  loguear("logs/error.log", "a+", "Metodo Utilizado: GET");
  loguear("logs/error.log", "a+", "Status Code Devuelto: ".$info['http_code']);
  echo "Ha ocurrido un error";
}

curl_close($ch);

