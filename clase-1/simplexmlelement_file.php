<?php

$xml_obj = new simpleXMLElement('notas.xml', null, true);//Hacemos una instancia de la clase simpleXMLElement -> ejecuta __construct

echo "<h1>SimpleXmlElement leyendo notas.xml</h1>";

if (is_object($xml_obj)) {

  $cantidad_notas = count($xml_obj->nota);

  for ($i = 0; $i < $cantidad_notas; $i++) {
  
    echo "<h5>Para: ".$xml_obj->nota[$i]->para."</h5>";
    echo "<h5>De: ".$xml_obj->nota[$i]->de."</h5>";
    echo "<h5>Titulo: ".$xml_obj->nota[$i]->titulo."</h5>";
    echo "<h5>Cuerpo: ".$xml_obj->nota[$i]->cuerpo."</h5>";
    echo "<h5>Fecha: ".$xml_obj->nota[$i]->fecha."</h5>";
    echo "<hr/>";
  }
}

