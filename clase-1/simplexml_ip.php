<?php

ini_set('date.timezone', 'America/Argentina/Buenos_Aires');

$url = 'http://www.geoplugin.net/xml.gp?ip='.$_SERVER['REMOTE_ADDR'];
$xml = file_get_contents($url);

if ($xml) { 
  $data = simplexml_load_string($xml);

  if (is_object($data)) {

	var_dump($data);

  } else {
    //echo "Error";
  }
} else {

  $date = new DateTime();
  $format = $date->format("Y-m-d H:i:s");
  $fp = fopen("logs/error.log", "a+");
  fwrite($fp, "[".$format."]\tError al cargar URL ".$url.PHP_EOL);//PHP_EOL \n \r\n
  fclose($fp);
  echo "Ha ocurrido un error";
}
