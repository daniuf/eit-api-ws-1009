<?php

$libros = array(
	    array(
		'isbn' => '12345678900987',
		'titulo' => 'Harry Potter',
		'autor' => 'Rowling',
		'anio_publicacion' => 2008,
		'img_portada' => 'portada.jpg'
	    ),
	    array(
		'isbn' => '123456789877661',
		'titulo' => 'Harry Potter 7',
		'autor' => 'Rowling 77',
		'anio_publicacion' => "2019",
		'img_portada' => 'portada_harry.jpg'
	     )
	  );

$xml = new SimpleXmlElement('<?xml version="1.0" encoding="utf-8"?><libros></libros>');
for ($i = 0; $i < count($libros); $i++) {
  $libro = $xml->addChild("libro");
  $libro->addAttribute('isbn', $libros[$i]['isbn']);
  $libro->addChild('titulo', $libros[$i]['titulo']);
  $libro->addChild('autor', $libros[$i]['autor']);
  $libro->addChild('img_portada', $libros[$i]['img_portada']);
  $libro->addChild('anio_publicacion', $libros[$i]['anio_publicacion']);
}

/**
$libro2 = $xml->addChild("libro");
$libro2->addAttribute('isbn', '12345678900988');
$libro2->addChild('titulo', 'Harry Potter 2');
$libro2->addChild('autor', 'Rowling');
$libro2->addChild('img_portada', 'portada.jpg');
$libro2->addChild('anio_publicacion', '2009');
**/

echo header("Content-Type: text/xml");
echo $xml->asXML();
