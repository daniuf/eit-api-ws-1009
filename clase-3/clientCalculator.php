<?php

//ini_set('soap.wsdl_cache', 0); => Por si quisieran deshabilitar el cache del documento WSDL

define("WSDL", "http://www.dneonline.com/calculator.asmx?WSDL");

try {
  //Intenta ejecutar lo de aqui dentro
  $options = array(
		'soap_version' => SOAP_1_1,
		'trace' => true
		);

  $soapclient = new SoapClient(WSDL, $options);

  /**
  $Add['intA'] = 3;
  $Add['intB'] = 9;
  $response = $soapclient->Add($Add);
  **/
  $Add['intA'] = 13;
  $Add['intB'] = 10;

  $response = $soapclient->__soapCall("Add", array($Add));

  echo "El resultado de la suma es ".$response->AddResult;

  echo "<hr/>";

  echo $soapclient->__getLastRequestHeaders().PHP_EOL;
  echo $soapclient->__getLastRequest().PHP_EOL;
  echo $soapclient->__getLastResponseHeaders().PHP_EOL;
  echo $soapclient->__getLastResponse().PHP_EOL;

} catch (Exception $e) {
  //Capturar el error
  echo "Ha ocurrido un error ".$e->getMessage();

}
