<?php

class Servidor {

  public function __construct() {}

  public function holaMundo() {
    return 'Hola mundo';
  }

  public function holaMundo2($mensaje) {
    return 'Hola, '.$mensaje;
  }
}

$options = array('uri' => 'http://eit.indianadev.biz/clase-3/servidor.php');
$server = new SoapServer(null, $options);
$server->setClass('Servidor');
$server->handle();
