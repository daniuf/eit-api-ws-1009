<?php

define("WSDL", "http://www.dneonline.com/calculator.asmx?WSDL");
define("WS", "http://www.dneonline.com/calculator.asmx");

try {
  //Intenta ejecutar lo de aqui dentro
  $options = array(
		'soap_version' => SOAP_1_1,
		'trace' => true
		);

  $soapclient = new SoapClient(WSDL, $options);
  $response = $soapclient->__getFunctions();

  var_dump($response);

  echo "<hr/>";

  echo $soapclient->__getLastRequestHeaders().PHP_EOL;
  echo $soapclient->__getLastRequest().PHP_EOL;
  echo $soapclient->__getLastResponseHeaders().PHP_EOL;
  echo $soapclient->__getLastResponse().PHP_EOL;

} catch (Exception $e) {
  //Capturar el error
  echo "Ha ocurrido un error ".$e->getMessage();

}
