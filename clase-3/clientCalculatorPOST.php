<?php

ini_set('date.timezone', 'America/Argentina/Buenos_Aires');

define("WS", "http://www.dneonline.com/calculator.asmx");

$method = 'POST';

$intA = 5;
$intB = 4;

$xml_input = '<?xml version="1.0" encoding="UTF-8"?>
<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns1="http://tempuri.org/"><SOAP-ENV:Body><ns1:Add><ns1:intA>'.$intA.'</ns1:intA><ns1:intB>'.$intB.'</ns1:intB></ns1:Add></SOAP-ENV:Body></SOAP-ENV:Envelope>';

$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, WS);
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_input);
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: text/xml', 'SOAPAction: "http://tempuri.org/Add"'));
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

$respuesta = curl_exec($ch);//Ejecuto peticion

$info = curl_getinfo($ch);//Pido info de esa peticion

//var_dump($info);

if ($info['http_code'] == 200) {

  $xml_respuesta = str_replace('soap:', '', $respuesta);
  $xml = simplexml_load_string($xml_respuesta);

  echo "El resultado de la suma es ".$xml->Body->AddResponse->AddResult;

} else {
  #loguear("logs/error.log", "a+", "Ha ocurrido un error al realizar la peticion a ".$url);
  #loguear("logs/error.log", "a+", "Metodo Utilizado: ".$method);
  #loguear("logs/error.log", "a+", "Status Code Devuelto: ".$info['http_code']);
  echo "Error al crear recurso";
}

curl_close($ch);

