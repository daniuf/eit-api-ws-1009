<?php

define("WS", "http://eit.indianadev.biz/clase-3/servidor.php");

try {
  //Intenta ejecutar lo de aqui dentro
  $options = array(
		'location' => WS,
		'uri' => WS,
		'trace' => true
		);

  $soapclient = new SoapClient(null, $options);
  //$respuesta = $soapclient->holaMundo();
  $respuesta = $soapclient->holaMundo2('Este mensaje es el parametro que ingresa al metodo holamundo2');

  echo "La respuesta del WS es ".$respuesta;

  echo $soapclient->__getLastRequestHeaders().PHP_EOL;
  echo $soapclient->__getLastRequest().PHP_EOL;
  echo $soapclient->__getLastResponseHeaders().PHP_EOL;
  echo $soapclient->__getLastResponse().PHP_EOL;

} catch (Exception $e) {
  //Capturar el error
  echo "Ha ocurrido un error ".$e->getMessage();

}
