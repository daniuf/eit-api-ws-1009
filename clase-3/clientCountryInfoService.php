<?php

define("WSDL", "http://webservices.oorsprong.org/websamples.countryinfo/CountryInfoService.wso?WSDL");

try {
  //Intenta ejecutar lo de aqui dentro
  $options = array(
		'soap_version' => SOAP_1_1,
		'trace' => true
		);

  $soapclient = new SoapClient(WSDL, $options);

  $CountryName['sCountryISOCode'] = 'AR';
  $response = $soapclient->CountryName($CountryName);

  var_dump($response);

  echo "<hr/>";

  echo $soapclient->__getLastRequestHeaders().PHP_EOL;
  echo $soapclient->__getLastRequest().PHP_EOL;
  echo $soapclient->__getLastResponseHeaders().PHP_EOL;
  echo $soapclient->__getLastResponse().PHP_EOL;

} catch (Exception $e) {
  //Capturar el error
  echo "Ha ocurrido un error ".$e->getMessage();

}
